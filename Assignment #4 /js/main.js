/**
 * Created by David D on 3/28/15.
 */

$(function(){
    console.log("Document is ready");
    window.setInterval(function(){
        console.log("TICK");
    },10000);

    $("#push").click(function(){
        console.log("Thanks for clicking!");
        $.getJSON("http://cs120.liucs.net/assn4/messages.json",
                    null,
                    function(data, status, xhr){
                        console.log(data);
                    })

    });

    $("#post").click(function(){
        $.post('http://cs120.liucs.net/assn4/messages.json',
                JSON.stringify({sender:$("#n").val(), text:$("#d").val(), mood:$("#m").val()}),
        function(){
            console.log("Success!");
        });
    });

});


