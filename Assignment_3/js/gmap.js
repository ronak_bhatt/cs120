/**
 * Created by RonakB on 3/5/15.
 */

console.log("Loading the function");

var map;

function initialize() {
    var mapOptions = {
        center: { lat: 41.875671, lng: -87.629845 },
        zoom: 10
    };
     map = new google.maps.Map(document.getElementById('map-canvas'),
        mapOptions);

    //var marker = new google.maps.Marker({
    //    position: {lat: 40.6928, lng: -73.9903},
    //    map: map,
    //    title:"Something is here!!"
    //});
    //google.maps.event.addListener(marker,'click', function(){
    //   console.log("You clicked");
    //});
}
google.maps.event.addDomListener(window, 'load', initialize);

var showMeChicago = function(){
    var ssPosition ={lat: 41.875671, lng: -87.629845}; // 41.875671, -87.629845
    var marker = new google.maps.Marker({
        position: ssPosition,
        map: map,
        title: "Bulls!!"
    });
    map.setCenter(ssPosition);
    map.setZoom(11);
}

var showMeFlorida = function(){
    var ssPosition ={lat: 28.556383, lng: -81.366586}; // 28.556383, -81.366586
    var marker = new google.maps.Marker({
        position: ssPosition,
        map: map,
        title: "Florida"
    });
    map.setCenter(ssPosition);
    map.setZoom(11);
}

var showMeKashmir = function(){
    var ssPosition ={lat: 34.086352, lng: 74.807367}; // 34.086352, 74.807367
    var marker = new google.maps.Marker({
        position: ssPosition,
        map: map,
        title: "Kashmir"
    });
    map.setCenter(ssPosition);
    map.setZoom(11);
}

var showMeLondon = function(){
    var ssPosition ={lat: 51.519670, lng: -0.126661}; // 51.519670, -0.126661
    var marker = new google.maps.Marker({
        position: ssPosition,
        map: map,
        title: "London"
    });
    map.setCenter(ssPosition);
    map.setZoom(11);
}

var showMeAgra = function(){
    var ssPosition ={lat: 27.175483, lng: 78.042150}; // 27.175483, 78.042150
    var marker = new google.maps.Marker({
        position: ssPosition,
        map: map,
        title: "Taj Mahal"
    });
    map.setCenter(ssPosition);
    map.setZoom(11);
}
