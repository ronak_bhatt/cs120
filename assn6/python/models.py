from django.db import models

# Create your models here.
class Message(models.Model):
    author = models.CharField(max_length=100)
    text = models.TextField()
    sent = models.DateTimeField()

    def __str__(self):
        return self.author + ": " + self.text

class Person(models.Model):
    first_name = models.CharField(max_length=80)
    last_name = models.CharField(max_length=80)
    job_title = models.CharField(max_length=80, blank=True)
    phone_number = models.CharField(max_length=15, blank=True)
    bio = models.TextField(blank=True)
    dob = models.DateField(blank=True, null=True)
    image = models.URLField(blank=True, null=True)

    def __str__(self):
        return self.last_name + ", " + self.first_name + ": " + self.job_title + "(" + self.bio[:25] + ")" +" # " + self.phone_number

class Department(models.Model):
    dept_name = models.CharField(max_length=80)
    dept_location = models.CharField(max_length=80)
    dept_supervisor = models.CharField(max_length=80)
    dept_bio = models.TextField(blank=True)
    dept_image = models.URLField(blank=True, null=True)

    def __str__(self):
        return self.dept_name + "; Supervisor: " + self.dept_supervisor


class Publication(models.Model):
    year = models.IntegerField()
    title = models.CharField(max_length=100)
    publisher = models.CharField(max_length=100)
    author = models.ForeignKey(Person)

    def __str__(self):
        return self.title + "/" + str(self.year)