# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('python', '0006_department_image'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='department',
            name='image',
        ),
    ]
