# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('python', '0005_publication_author'),
    ]

    operations = [
        migrations.AddField(
            model_name='department',
            name='image',
            field=models.URLField(null=True, blank=True),
        ),
    ]
