# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('python', '0002_auto_20150504_1923'),
    ]

    operations = [
        migrations.AddField(
            model_name='person',
            name='image',
            field=models.URLField(null=True, blank=True),
        ),
    ]
