# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('python', '0009_remove_department_image'),
    ]

    operations = [
        migrations.AddField(
            model_name='department',
            name='dept_bio',
            field=models.TextField(blank=True),
        ),
    ]
