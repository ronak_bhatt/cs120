# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('python', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Department',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('dept_name', models.CharField(max_length=80)),
                ('dept_location', models.CharField(max_length=80)),
                ('dept_supervisor', models.CharField(max_length=80)),
            ],
        ),
        migrations.CreateModel(
            name='Message',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('author', models.CharField(max_length=100)),
                ('text', models.TextField()),
                ('sent', models.DateTimeField()),
            ],
        ),
        migrations.RenameField(
            model_name='person',
            old_name='birth_date',
            new_name='dob',
        ),
        migrations.AddField(
            model_name='person',
            name='job_title',
            field=models.CharField(max_length=80, blank=True),
        ),
        migrations.AddField(
            model_name='person',
            name='phone_number',
            field=models.CharField(max_length=15, blank=True),
        ),
        migrations.AlterField(
            model_name='person',
            name='first_name',
            field=models.CharField(max_length=80),
        ),
        migrations.AlterField(
            model_name='person',
            name='last_name',
            field=models.CharField(max_length=80),
        ),
    ]
