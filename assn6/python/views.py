from django.shortcuts import render
from django.http import HttpResponse
from python.models import Person
from python.models import Department
from python.models import Publication

# Create your views here.

def homepage(request):
    #return HttpResponse("Hello <b>World!</b> <a href='/junk'>Go to junk</a>")
    numPeople = Person.objects.count()
    people = Person.objects.all()
    numDept = Department.objects.count()
    dept = Department.objects.all()
    return render(request, 'homepage.html',
                  {'numPeople': numPeople,
                   'people': people,
                   'numDepartment': numDept,
                   'department': dept})

def deptpage(request, id):
    d = Department.objects.get(id=id)
    return render(request, 'department.html', {'d':d})

def profilepage(request,id):
    p = Person.objects.get(id=id)
    pubs = Publication.objects.filter(author = p)
    return render(request,'person.html', {'p':p, 'pubs': pubs})

def junk(request):
    return HttpResponse("Another page... <a href='/'>Go to Home</a>")