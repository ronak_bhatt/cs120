from django.contrib import admin
from python.models import Message
from python.models import Person
from python.models import Department
from python.models import Publication
# Register your models here.
admin.site.register(Message)
admin.site.register(Person)
admin.site.register(Department)
admin.site.register(Publication)