from django.conf.urls import include, url
from django.contrib import admin

urlpatterns = [
    # Examples:
    # url(r'^$', 'PyCharm.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^$', 'python.views.homepage', name='home'),
    url(r'^dept/([0-9]+)$', 'python.views.deptpage'),
    url(r'^profile/([0-9]+)$', 'python.views.profilepage'),
    url(r'^junk$', 'python.views.junk'),
    url(r'^admin/', include(admin.site.urls)),
]
