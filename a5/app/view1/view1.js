'use strict';

angular.module('myApp.view1', ['ngRoute'])

    .config(['$routeProvider', function($routeProvider) {
        $routeProvider.when('/view1', {
            templateUrl: 'view1/view1.html',
            controller: 'View1Ctrl'
        });
    }])

    .controller('View1Ctrl', ['$scope', '$filter', function($scope) {
        $scope.taskList = [];
        $scope.addTask = function(){
            var taskObject = { title:$scope.title, priority: $scope.priority, dueDate: $scope.dueDate,
                finished: $scope.finished}
            $scope.taskList.push(taskObject);
            $scope.dueDate="";
            $scope.task="";
            $scope.title="";
            $scope.priority="";
            $scope.finished="";
        };
        $scope.remove = function(task) {
            console.log("Removing", task);
            $scope.taskList = $scope.taskList.filter(function(t){return t !== task});
        };
        $scope.edit = function(i){
            $scope.taskList[i].title = $scope.title;
            $scope.taskList[i].dueDate = $scope.dueDate;
            $scope.taskList[i].priority = $scope.priority;
        };
    }]);
